console.log('setting up ts-node...');

// set delays to lowest possible
process.env.DELAY_MIN = '1';
process.env.DELAY_MAX = '1';

require('tsconfig-paths/register');
require('ts-node').register({
    compiler: 'ttypescript',
    cache: false,
    compilerOptions: {
        removeComments: false
    }
});
