import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import * as HttpStatus from 'http-status-codes';
import Db from 'src/database';
import { API } from 'src/definitions';
import buildApiEndpointFn from 'test/buildApiEndpointFn';
import setupTestDb, { testyToken, testyUser } from 'test/setupTestDb';
import { keys } from 'ts-transformer-keys/index';
import app = require('src/app');

chai.use(chaiHttp);
describe('routes/oauth', function () {
    beforeEach(() => setupTestDb());

    // region RequestAccessToken (POST /token)
    describe('RequestAccessToken', function () {
        const buildApiEndpoint = buildApiEndpointFn<API.Authorization.RequestAccessToken.Parameters, API.Authorization.RequestAccessToken.ENDPOINT>('/oauth/token');

        // region it should return w/ "unsupported_grant_type" when request is missing a grant_type property
        it('should return w/ "unsupported_grant_type" when given a grant_type that isn\'t supported',
            /**
             * Tests that a POST request to `/oauth/token` where:
             *      * the `body` doesn't have a `grant_type` property,
             *
             * Returns a response with `status` `"BAD_REQUEST"`, and a `body` made up of:
             *      * a `string` property "error", with a value of "unsupported_grant_type", and
             *      * a `string` property "error_description", with a non-empty value.
             */
            async function () {
                // region request
                const res = await chai.request(app)
                                      .post(buildApiEndpoint());
                // endregion

                expect(res).to.be.json.with.status(HttpStatus.BAD_REQUEST);

                expect(res.body).to.be.an('object');
                expect(res.body.error).to.equal('unsupported_grant_type');
                expect(res.body.error_description).to.be.a('string');
            }
        );
        // endregion
        // region it should return w/ "unsupported_grant_type" when given a grant_type that isn't supported
        it('should return w/ "unsupported_grant_type" when given a grant_type that isn\'t supported',
            /**
             * Tests that a POST request to `/oauth/token` where:
             *      * the `body` has a `grant_type` property that isn't supported,
             *
             * Returns a response with `status` `"BAD_REQUEST"`, and a `body` made up of:
             *      * a `string` property "error", with a value of "unsupported_grant_type", and
             *      * a `string` property "error_description", with a non-empty value.
             */
            async function () {
                const data = {
                    client_id: 1,
                    grant_type: 'unsupported',
                    scope: '*'
                };

                // region request
                const res = await chai.request(app)
                                      .post(buildApiEndpoint())
                                      .send(data);
                // endregion

                expect(res).to.be.json.with.status(HttpStatus.BAD_REQUEST);

                expect(res.body).to.be.an('object');
                expect(res.body.error).to.equal('unsupported_grant_type');
                expect(res.body.error_description).to.be.a('string');
            }
        );
        // endregion
        describe('"password" grant_type', function () {
            // region it should return w/ "invalid_grant" when given an invalid username
            it('should return w/ "invalid_grant" when given an invalid username',
                /**
                 * Tests that a POST request to `/oauth/token` where:
                 *      * the `body` has a `grant_type` property containing the value of 'password', and
                 *      * the `body` has a `username` property containing an invalid email that is not in the database,
                 *
                 * Returns a response with `status` `"BAD_REQUEST"`, and a `body` made up of:
                 *      * a `string` property "error", with a value of "invalid_grant", and
                 *      * a `string` property "error_description", with a non-empty value.
                 */
                async function () {
                    const data: API.Authorization.RequestAccessToken.PasswordGrantRequest = {
                        client_id: 1,
                        grant_type: 'password',
                        username: 'invalid-email',
                        password: 'my-password',
                        scope: '*'
                    };

                    // region request
                    const res = await chai.request(app)
                                          .post(buildApiEndpoint())
                                          .send(data);
                    // endregion

                    expect(res).to.be.json.with.status(HttpStatus.BAD_REQUEST);

                    expect(res.body).to.be.an('object');
                    expect(res.body.error).to.equal('invalid_grant');
                    expect(res.body.error_description).to.be.a('string');
                }
            );
            // endregion
            // region it should return w/ "invalid_grant" when given an invalid password
            it('should return w/ "invalid_grant" when given an invalid password',
                /**
                 * Tests that a POST request to `/oauth/token` where:
                 *      * the `body` has a `grant_type` property containing the value of 'password', and
                 *      * the `body` has a `username` property containing a valid email for a user that exists in the database, and
                 *      * the `body` has a `password` property containing a value that is not the password for the given user,
                 *
                 * Returns a response with `status` `"BAD_REQUEST"`, and a `body` made up of:
                 *      * a `string` property "error", with a value of "invalid_grant", and
                 *      * a `string` property "error_description", with a non-empty value.
                 */
                async function () {
                    const data: API.Authorization.RequestAccessToken.PasswordGrantRequest = {
                        client_id: 1,
                        grant_type: 'password',
                        username: testyUser.email,
                        password: 'not-my-password',
                        scope: '*'
                    };

                    // region request
                    const res = await chai.request(app)
                                          .post(buildApiEndpoint())
                                          .send(data);
                    // endregion

                    expect(res).to.be.json.with.status(HttpStatus.BAD_REQUEST);

                    expect(res.body).to.be.an('object');
                    expect(res.body.error).to.equal('invalid_grant');
                    expect(res.body.error_description).to.be.a('string');
                }
            );
            // endregion
            // region it should return w/ an access token when given valid credentials
            it('should return w/ an access token when given valid credentials',
                /**
                 * Tests that a POST request to `/oauth/token` where:
                 *      * the `body` has a `grant_type` property containing the value of 'password', and
                 *      * the `body` has a `username` property containing a valid email for a user that exists in the database, and
                 *      * the `body` has a `password` property containing the correct password for the user with the email being used,
                 *
                 * Returns a response with `status` `"OK"`, and a `body` made up of:
                 *      * an {@link OAuth2Token OAuth2Token}, with `access_token` & `refresh_token` properties.
                 *
                 * The database should contain the new token, stored against id of the `User` that made the request.
                 */
                async function () {
                    const data: API.Authorization.RequestAccessToken.PasswordGrantRequest = {
                        client_id: 1,
                        grant_type: 'password',
                        username: testyUser.email,
                        password: testyUser.password,
                        scope: '*'
                    };

                    // region request
                    const res = await chai.request(app)
                                          .post(buildApiEndpoint())
                                          .send(data);
                    // endregion

                    expect(res).to.be.json.with.status(HttpStatus.OK);

                    // region expect body
                    const body = res.body as API.Authorization.RequestAccessToken.Response;

                    expect(body).to.be.an('object').and.have.keys(keys<typeof body>());
                    expect(body.refresh_token).to.be.a('string');
                    expect(body.access_token).to.be.a('string');
                    expect(body.expires_in).to.be.a('number');
                    expect(body.token_type).to.equal('Bearer');
                    // endregion
                    // region expect db
                    const dbToken = Db.tokens.data[Db.tokens.data.length - 1];

                    expect(dbToken).to.be.an('object').and.have.keys(keys<typeof dbToken & { $loki: unknown }>());
                    expect(dbToken.userId).to.equal(testyUser.id);
                    expect(dbToken.access_token).to.equal(body.access_token);
                    expect(dbToken.refresh_token).to.equal(body.refresh_token);
                    expect(dbToken.updatedAt).to.be.null;
                    expect(dbToken.deletedAt).to.be.null;
                    expect(dbToken.scope).to.equal(data.scope);
                    // endregion
                }
            );
            // endregion
        });
        describe('"refresh_token" grant_type', function () {
            // region it should return w/ "invalid_grant" when given an invalid refresh_token
            it('should return w/ "invalid_grant" when given an invalid refresh_token',
                /**
                 * Tests that a POST request to `/oauth/token` where:
                 *      * the `body` has a `grant_type` property containing the value of 'refresh_token', and
                 *      * the `body` has a `refresh_token` property containing an invalid value that's not in the database,
                 *
                 * Returns a response with `status` `"BAD_REQUEST"`, and a `body` made up of:
                 *      * a `string` property "error", with a value of "invalid_grant", and
                 *      * a `string` property "error_description", with a non-empty value.
                 */
                async function () {
                    const data: API.Authorization.RequestAccessToken.RefreshTokenGrantRequest = {
                        client_id: 1,
                        grant_type: 'refresh_token',
                        refresh_token: 'invalid-refresh-token',
                        scope: '*'
                    };

                    // region request
                    const res = await chai.request(app)
                                          .post(buildApiEndpoint())
                                          .send(data);
                    // endregion

                    expect(res).to.be.json.with.status(HttpStatus.BAD_REQUEST);

                    expect(res.body).to.be.an('object');
                    expect(res.body.error).to.equal('invalid_grant');
                    expect(res.body.error_description).to.be.a('string');
                }
            );
            // endregion
            // region it should return w/ a new access token when given a valid refresh_token
            it('should return w/ a new access token when given a valid refresh_token',
                /**
                 * Tests that a POST request to `/oauth/token` where:
                 *      * the `body` has a `grant_type` property containing the value of 'refresh_token', and
                 *      * the `body` has a `refresh_token` property containing an valid value that exists in the database,
                 *
                 * Returns a response with `status` `"OK"`, and a `body` made up of:
                 *      * a *new* {@link OAuth2Token OAuth2Token}, with `access_token` & `refresh_token` properties.
                 *
                 * The database should update the relevant `AccessToken` entity with the new `access_token` & `refresh_token`,
                 * along with the `updatedAt` value of the entity.
                 */
                async function () {
                    const data: API.Authorization.RequestAccessToken.RefreshTokenGrantRequest = {
                        client_id: 1,
                        grant_type: 'refresh_token',
                        refresh_token: testyToken.refresh_token,
                        scope: '*'
                    };

                    // region request
                    const res = await chai
                        .request(app)
                        .post(buildApiEndpoint())
                        .send(data);
                    // endregion

                    expect(res).to.be.json.with.status(HttpStatus.OK);

                    // region expect body
                    const body = res.body as API.Authorization.RequestAccessToken.Response;

                    expect(body).to.be.an('object').and.have.keys(keys<typeof body>());
                    expect(body.refresh_token).to.be.a('string').and.not.equal(testyToken.refresh_token);
                    expect(body.access_token).to.be.a('string').and.not.equal(testyToken.access_token);
                    expect(body.expires_in).to.be.a('number');
                    expect(body.token_type).to.equal('Bearer');
                    // endregion
                    // region expect db
                    const dbToken = Db.tokens.data[Db.tokens.data.length - 1];

                    expect(dbToken).to.be.an('object').and.have.keys(keys<typeof dbToken & { $loki: unknown }>());
                    expect(dbToken.id).to.equal(testyToken.id);
                    expect(dbToken.userId).to.equal(testyUser.id);
                    expect(dbToken.access_token).to.equal(body.access_token);
                    expect(dbToken.refresh_token).to.equal(body.refresh_token);
                    expect(dbToken.updatedAt).to.be.a('string').and.not.equal(testyToken.updatedAt);
                    expect(dbToken.deletedAt).to.be.null;
                    expect(dbToken.scope).to.equal(data.scope);
                    // endregion
                }
            );
            // endregion
        });
    });
    // endregion
});
