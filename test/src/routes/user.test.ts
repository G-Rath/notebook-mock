import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import * as HttpStatus from 'http-status-codes';
import { API } from 'src/definitions';
import buildApiEndpointFn from 'test/buildApiEndpointFn';
import setupTestDb, { testyToken, testyUser } from 'test/setupTestDb';
import { keys } from 'ts-transformer-keys/index';
import app = require('src/app');

chai.use(chaiHttp);
describe('routes/user', function () {
    beforeEach(() => setupTestDb());

    // region GetCurrentUser (GET /api/user)
    describe('GetCurrentUser', function () {
        const buildApiEndpoint = buildApiEndpointFn<API.Users.GetCurrentUser.Parameters, API.Users.GetCurrentUser.ENDPOINT>('/api/user');

        // region it should return status "UNAUTHORIZED" w/ empty body
        it('should return status UNAUTHORIZED w/ empty body when missing bearer token',
            /**
             * Tests that a POST request to `/api/user` where:
             *      * there is no bearer authentication token.
             *
             * Returns a response with `status` `"UNAUTHORIZED"`, and an empty `body`.
             */
            async function () {
                // region request
                const res = await chai.request(app)
                                      .get(buildApiEndpoint());
                // endregion

                expect(res).to.be.json.with.status(HttpStatus.UNAUTHORIZED);

                expect(res.body).to.be.empty;
            }
        );
        // endregion
        // region it should return status "UNAUTHORIZED" w/ empty body when access_token is invalid
        it('should return status "UNAUTHORIZED" w/ empty body when access_token is invalid',
            /**
             * Tests that a POST request to `/api/user` where:
             *      * there is an invalid bearer `access_token`
             *
             * Returns a response with `status` `"UNAUTHORIZED"`, and an empty `body`.
             */
            async function () {
                // region request
                const res = await chai.request(app)
                                      .get(buildApiEndpoint())
                                      .auth('invalid', { type: 'bearer' });
                // endregion

                expect(res).to.be.json.with.status(HttpStatus.UNAUTHORIZED);

                expect(res.body).to.be.empty;
            }
        );
        // endregion
        // region it should return status OK w/ current user in body
        it('should return status OK w/ current user in body',
            /**
             * Tests that a POST request to `/api/user` where:
             *      * there is a valid bearer `access_token`
             *
             * Returns a response with `status` `"OK"`, and a `body` made up of:
             *      * a 'data' property, containing the current user.
             */
            async function () {
                // region request
                const res = await chai.request(app)
                                      .get(buildApiEndpoint())
                                      .auth(testyToken.access_token, { type: 'bearer' });
                // endregion

                expect(res).to.be.json.with.status(HttpStatus.OK);
                expect(res.body).to.be.an('object');

                // region expect body.data
                const bodyData = res.body.data as API.Users.GetCurrentUser.Response['data'];

                expect(bodyData).to.be.an('object').and.have.keys(keys<typeof bodyData>());
                expect(bodyData.id).to.be.a('string').and.to.equal(testyUser.id);
                expect(bodyData.email).to.be.a('string').and.to.equal(testyUser.email);
                expect(bodyData.firstName).to.be.a('string').and.to.equal(testyUser.firstName);
                expect(bodyData.lastName).to.be.a('string').and.to.equal(testyUser.lastName);
                expect(bodyData.pages).to.be.an('array');
                expect(bodyData.notebooks).to.be.an('array');
                expect(bodyData.createdAt).to.be.a('string').and.to.equal(testyUser.createdAt);
                expect(bodyData.updatedAt).to.be.null.and.to.equal(testyUser.updatedAt);
                expect(bodyData.deletedAt).to.be.null.and.to.equal(testyUser.deletedAt);
                // endregion
            }
        );
        // endregion
        // todo-test: what happens when using an access token for a user that's deleted
    });
    // endregion
});
