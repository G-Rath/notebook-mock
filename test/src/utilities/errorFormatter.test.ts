import { expect } from 'chai';
import { UnformattedError } from 'src/definitions';
import errorFormatter from 'src/utilities/errorFormatter';
import { keys } from 'ts-transformer-keys/index';

describe('utilFn: errorFormatter', function () {
    // region it should return a well-formatted error object
    it('should return a well-formatted error object', function () {
        const rawError: UnformattedError = {
            location: 'body',
            param: 'firstName',
            value: '',
            msg: 'must only contain letters'
        };

        const formattedError = errorFormatter(rawError);

        expect(formattedError).to.be.an('object').and.have.keys(keys<typeof formattedError>());
        expect(formattedError.param).to.be.equal(rawError.param);
        expect(formattedError.message).to.be.equal(rawError.msg);
    });
    // endregion
});
