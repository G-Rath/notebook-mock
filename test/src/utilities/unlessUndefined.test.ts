import { expect } from 'chai';
import { API } from 'src/definitions';
import unlessUndefined from 'src/utilities/unlessUndefined';

type NullOrString = API.Nullable<string>;

describe('utilFn: unlessUndefined', function () {
    // region it should return "value" when "value" is a falsy value
    it('should return "value" when "value" is a falsy value', function () {
        const value = '';
        const unless = 'unless';

        const result = unlessUndefined<NullOrString>(value, unless);

        expect(result).to.equal(value);
    });
    // endregion
    // region it should return "value" when "value" is a truthy value
    it('should return "value" when "value" is a truthy value', function () {
        const value = 'value';
        const unless = 'unless';

        const result = unlessUndefined<NullOrString>(value, unless);

        expect(result).to.equal(value);
    });
    // endregion
    // region it should return "value" when "value" is null
    it('should return "value" when "value" is null', function () {
        const value = null;
        const unless = 'unless';

        const result = unlessUndefined<NullOrString>(value, unless);

        expect(result).to.equal(value);
    });
    // endregion
    // region it should return "unless" when "value" is undefined
    it('should return "unless" when "value" is undefined', function () {
        const value = undefined;
        const unless = 'unless';

        const result = unlessUndefined<NullOrString>(value, unless);

        expect(result).to.equal(unless);
    });
    // endregion
});
