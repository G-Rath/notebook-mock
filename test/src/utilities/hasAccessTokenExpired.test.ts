import { expect } from 'chai';
import hasAccessTokenExpired from 'src/utilities/hasAccessTokenExpired';

describe('utilFn: hasAccessTokenExpired', function () {
    const oneSecondInMs = 1000;
    const oneMinuteInMs = oneSecondInMs * 60;
    const oneHourInMs = oneMinuteInMs * 60;

    // region it should return "false" when expires_in + createdAt is greater than Date.now
    it('should return "false" when expires_in + createdAt is greater than Date.now', function () {
        const hasExpired = hasAccessTokenExpired({
            id: '1',
            userId: '1',
            token_type: 'Bearer',
            scope: '*',
            access_token: 'ignore',
            refresh_token: 'ignore',
            expires_in: 120,
            createdAt: (new Date(Date.now() - oneMinuteInMs)).toISOString(),
            updatedAt: null,
            deletedAt: null
        });

        expect(hasExpired).to.be.false;
    });
    // endregion
    // region it should return "false" when expires_in + updatedAt is greater than Date.now
    it('should return "false" when expires_in + updatedAt is greater than Date.now', function () {
        const hasExpired = hasAccessTokenExpired({
            id: '1',
            userId: '1',
            token_type: 'Bearer',
            scope: '*',
            access_token: 'ignore',
            refresh_token: 'ignore',
            expires_in: 120,
            createdAt: (new Date(Date.now() - oneHourInMs)).toISOString(),
            updatedAt: (new Date(Date.now() - oneMinuteInMs)).toISOString(),
            deletedAt: null
        });

        expect(hasExpired).to.be.false;
    });
    // endregion
    // region it should return "true" when expires_in + createdAt is less than Date.now
    it('should return "true" when expires_in + createdAt is less than Date.now', function () {
        const hasExpired = hasAccessTokenExpired({
            id: '1',
            userId: '1',
            token_type: 'Bearer',
            scope: '*',
            access_token: 'ignore',
            refresh_token: 'ignore',
            expires_in: 120,
            createdAt: (new Date(Date.now() - oneHourInMs)).toISOString(),
            updatedAt: null,
            deletedAt: null
        });

        expect(hasExpired).to.be.true;
    });
    // endregion
    // region it should return "true" when expires_in + updatedAt is less than Date.now
    it('should return "true" when expires_in + updatedAt is less than Date.now', function () {
        const hasExpired = hasAccessTokenExpired({
            id: '1',
            userId: '1',
            token_type: 'Bearer',
            scope: '*',
            access_token: 'ignore',
            refresh_token: 'ignore',
            expires_in: 120,
            createdAt: (new Date(Date.now() - oneHourInMs)).toISOString(),
            updatedAt: (new Date(Date.now() - oneMinuteInMs * 3)).toISOString(),
            deletedAt: null
        });

        expect(hasExpired).to.be.true;
    });
    // endregion
    // region it should return "true" when expires_in + createdAt is equal to Date.now
    it('should return "true" when expires_in + updatedAt is equal to Date.now', function () {
        const hasExpired = hasAccessTokenExpired({
            id: '1',
            userId: '1',
            token_type: 'Bearer',
            scope: '*',
            access_token: 'ignore',
            refresh_token: 'ignore',
            expires_in: 120,
            createdAt: (new Date(Date.now() - oneMinuteInMs * 2)).toISOString(),
            updatedAt: null,
            deletedAt: null
        });

        expect(hasExpired).to.be.true;
    });
    // endregion
});
