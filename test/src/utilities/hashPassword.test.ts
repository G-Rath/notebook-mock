import { expect } from 'chai';
import hashPassword from 'src/utilities/hashPassword';

describe('utilFn: hashPassword', function () {
    const password = 'my-password';
    const salt = 'my-salt';
    const expectedHash = 'k7x7QWo0egDByAAAOCIu+SMCNPBB4d4339i9EomPGeU=';

    // region it should return the expected hash
    it('should return the expected hash', function () {
        const resultingHash = hashPassword(password, salt);

        expect(resultingHash).to.equal(expectedHash);
    });
    // endregion
    // region it should return a different hash, given a different salt
    it('should return a different hash, given a different salt', function () {
        const resultingHash = hashPassword(password, 'not-my-salt');

        expect(resultingHash).to.not.equal(expectedHash);
    });
    // endregion
    // region it should return a different hash, given a different password
    it('should return a different hash, given a different password', function () {
        const resultingHash = hashPassword('not-my-password', salt);

        expect(resultingHash).to.not.equal(expectedHash);
    });
    // endregion
});
