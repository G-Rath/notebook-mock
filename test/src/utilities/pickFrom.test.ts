import { expect } from 'chai';
import pickFrom from 'src/utilities/pickFrom';

describe('utilFn: pickFrom', function () {
    const target = {
        hello: 'world',
        logFn: console.log,
        count: 3,
        settings: {
            logging: true,
            prefix: 'log: '
        }
    };

    // region it should return an object with no properties
    it('should return an object with no properties', function () {
        const keys: Array<keyof typeof target> = [];

        const result = pickFrom(target, keys);

        expect(result).to.be.empty;
    });
    // endregion
    // region it should return an object with 'hello' & 'logFn' properties
    it('should return an object with \'hello\' & \'logFn\' properties', function () {
        const keys: Array<keyof typeof target> = ['hello', 'logFn'];

        const result = pickFrom(target, keys);

        expect(result).to.be.an('object');
        keys.forEach(key => {
            expect(result).to.have.property(key);
            expect(result[key]).to.equal(target[key]);
        });
    });
    // endregion
});
