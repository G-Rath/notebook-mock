import { expect } from 'chai';
import omitFrom from 'src/utilities/omitFrom';

describe('utilFn: omitFrom', function () {
    const target = {
        hello: 'world',
        logFn: console.log,
        count: 3,
        settings: {
            logging: true,
            prefix: 'log: '
        }
    };

    // region it should return an object with all properties
    it('should return an object with all properties', function () {
        const keys: Array<keyof typeof target> = [];

        const result = omitFrom(target, keys);

        expect(Object.keys(result)).to.have.same.members(Object.keys(target));
    });
    // endregion
    // region it should return an object without 'hello' & 'logFn' properties
    it('should return an object without \'hello\' & \'logFn\' properties', function () {
        const keys: Array<keyof typeof target> = ['hello', 'logFn'];

        const result = omitFrom(target, keys);

        expect(result).to.be.an('object');
        keys.forEach(key => expect(result).to.not.have.property(key));
    });
    // endregion
});
