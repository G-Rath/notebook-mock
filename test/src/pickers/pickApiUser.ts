import { expect } from 'chai';
import { API, Omit } from 'src/definitions';
import pickApiUser from 'src/pickers/pickApiUser';
import { testyUser } from 'test/setupTestDb';
import { keys } from 'ts-transformer-keys/index';

describe('picker: pickApiUser', function () {
    // region it should return an object with only keys found in the API.Entities.User interface
    it('should return an object with only keys found in the API.Entities.User interface', function () {
        const pickedApiUser = pickApiUser(testyUser);

        expect(pickedApiUser).to.be.an('object');
        expect(pickedApiUser).to.have.keys(keys<API.Entities.User>())
                             .but.not.keys(keys<Omit<typeof testyUser, keyof API.Entities.User>>());
    });
    // endregion
});
