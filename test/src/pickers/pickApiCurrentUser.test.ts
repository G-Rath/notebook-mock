import { expect } from 'chai';
import { API, Omit } from 'src/definitions';
import pickApiCurrentUser from 'src/pickers/pickApiCurrentUser';
import { testyUser } from 'test/setupTestDb';
import { keys } from 'ts-transformer-keys/index';

describe('picker: pickApiCurrentUser', function () {
    // region it should return an object with only keys found in the API.Entities.CurrentUser interface
    it('should return an object with only keys found in the API.Entities.CurrentUser interface', function () {
        const pickedApiCurrentUser = pickApiCurrentUser(testyUser);

        expect(pickedApiCurrentUser).to.be.an('object');
        expect(pickedApiCurrentUser).to.have.keys(keys<API.Entities.CurrentUser>())
                                    .but.not.keys(keys<Omit<typeof testyUser, keyof API.Entities.CurrentUser>>());
    });
    // endregion
});
