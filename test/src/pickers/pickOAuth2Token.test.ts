import { expect } from 'chai';
import { OAuth2Token, Omit } from 'src/definitions';
import pickOAuth2Token from 'src/pickers/pickOAuth2Token';
import { testyToken } from 'test/setupTestDb';
import { keys } from 'ts-transformer-keys/index';

describe('picker: pickOAuth2Token', function () {
    // region it should return an object with only keys found in the OAuth2Token interface
    it('should return an object with only keys found in the OAuth2Token interface', function () {
        const pickedOAuth2Token = pickOAuth2Token(testyToken);

        expect(pickedOAuth2Token).to.be.an('object');
        expect(pickedOAuth2Token).to.have.keys(keys<OAuth2Token>())
                                 .but.not.keys(keys<Omit<typeof testyToken, keyof OAuth2Token>>());
    });
    // endregion
});
