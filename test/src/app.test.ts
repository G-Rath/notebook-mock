import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import * as HttpStatus from 'http-status-codes';
import setupTestDb from 'test/setupTestDb';
import app = require('src/app');

chai.use(chaiHttp);
describe('app', function () {
    beforeEach(() => setupTestDb());

    // region it should return status OK w/ empty body
    it('should return status OK w/ empty body',
        /**
         * Tests that a GET request to `/live` succeeds.
         *
         * The response should have the `status` code "OK",
         * and the responses `body` should:
         *      * be empty
         */
        async function () {
            // region request
            const res = await chai.request(app)
                                  .get('/live');
            // endregion

            expect(res).to.be.json.with.status(HttpStatus.OK);

            expect(res.body).to.be.empty;
        }
    );
    // endregion
    // region it should return status NOT_FOUND w/ empty body
    it('should return status NOT_FOUND w/ empty body',
        /**
         * Tests that a GET request to `/not-found` fails.
         *
         * The response should have the `status` code "NOT_FOUND",
         * and the responses `body` should:
         *      * be empty
         */
        async function () {
            // region request
            const res = await chai.request(app)
                                  .post('/not-found');
            // endregion

            // todo: for some reason this is html instead of json
            expect(res).to.be.html.with.status(HttpStatus.NOT_FOUND);

            expect(res.body).to.be.empty;
        }
    );
    // endregion
});
