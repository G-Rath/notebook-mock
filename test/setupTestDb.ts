import fs from 'fs';
import * as path from 'path';
import Db from 'src/database';
import { generateStringToken, hashPassword } from 'src/utilities';

process.env.DB_FILE_PATH = path.join(__dirname, 'test.json');

/**
 * The testy `User`, inserted for testing.
 *
 * Note the `password` property is the *original*, plain text password.
 * It's hashed before being inserted into the database.
 */
export const testyUser: Db.User = {
    id: `${Db.users.maxId}`,
    email: 'test@tester.com',
    firstName: 'Testy',
    lastName: 'Tester',
    salt: generateStringToken(),
    password: 'my-password',
    pages: [],
    notebooks: [],
    createdAt: (new Date()).toISOString(),
    updatedAt: null,
    deletedAt: null
};

/**
 * The testy `AccessToken`, inserted for testing.
 *
 * Note that the token doesn't conform to the security
 * standards used by the actual api when inserting tokens.
 */
export const testyToken: Db.AccessToken = {
    id: `${Db.users.maxId}`,
    userId: testyUser.id,
    access_token: 'access-me',
    refresh_token: 'refresh-me',
    token_type: 'Bearer',
    scope: '*',
    expires_in: 15,
    createdAt: (new Date()).toISOString(),
    updatedAt: null,
    deletedAt: null
};

const seedTestDb = () => {
    // update the createdAt properties of testy obj every seed
    testyUser.createdAt = (new Date()).toISOString();
    testyToken.createdAt = (new Date()).toISOString();

    const insertedTestyUser = Db.users.insertOne({
        ...testyUser,
        password: hashPassword(testyUser.password, testyUser.salt)
    });

    if (insertedTestyUser === undefined) {
        throw new Error('something went wrong inserting testing user');
    }

    const insertedTestyToken = Db.tokens.insertOne({
        ...testyToken
    });

    if (insertedTestyToken === undefined) {
        throw new Error('something went wrong interesting testing token');
    }
};

export default async () => {
    // todo: consider using a file..?
    if (fs.existsSync(Db.dbFilePath)) {
        fs.unlinkSync(Db.dbFilePath);
    }

    await Db.reconfigureDb({
        persistenceMethod: 'fs'
    });

    if (Db.users.data.length !== 0) {
        throw new Error('database hasn\'t been wiped!');
    }

    await seedTestDb();
    await Db.save();
}
