/**
 * Builds a function for building an api endpoint,
 * to ensure that all the required parameters for
 * the url are provided.
 *
 * @param {string} endpoint
 *
 * @return {(params?: Params) => string}
 *
 * @template Params
 * @template Endpoint
 */
export default <Params extends object, Endpoint extends string>(endpoint: Endpoint) => (params?: Params) => params ? Object.keys(params).reduce(
    (str, key) => str.replace(`:${key}`, params[key]),
    endpoint
) : endpoint;
