import { ExtractPropertiesOfType } from 'src/definitions';

/**
 * Groups an array of objects based on the value of a shared property of type `string`.
 *
 * @param {Array<T>} objects
 * @param {V} property
 * @param {string} defaultValue
 *
 * @return {Record<T[V], Array<T>>}
 *
 * @template T, V
 */
export default <T extends { [k: string]: any }, V extends keyof ExtractPropertiesOfType<T, PropertyKey | undefined>>(
    objects: Array<T>,
    property: V,
    defaultValue: T[V]
): Record<T[V], Array<T>> => {
    const map: Record<PropertyKey, Array<T>> = {};

    objects.forEach(obj => {
        const value = obj[property] || defaultValue;

        if (!map[value]) {
            map[value] = [];
        }

        map[value].push(obj);
    });

    return map;
};
