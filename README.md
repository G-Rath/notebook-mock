# @g-rath/notebook-mock
A lightweight notebook application.

This is the mock-server, for mocking the api. 

## Setup
Copy `.env.example` to `.env`, and fill out the required values.

Run `npm install`.
