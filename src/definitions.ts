import { Location } from 'express-validator/check/location';

// region ExtractPropertiesOfType
type FilterFlags<O, T> = { [K in keyof O]: O[K] extends T ? K : never };
type AllowedNames<O, T> = FilterFlags<O, T>[keyof O];

/**
 * Extract all properties from `O` that are of type `T`.
 */
export type ExtractPropertiesOfType<O, T> = Pick<O, AllowedNames<O, T>>;

// endregion

export type ExcludeOptionalProperties<T> = Pick<T, {
    [K in keyof T]-?: {} extends { [P in K]: T[K] } ? never : K
}[keyof T]>

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

export interface UnformattedError {
    location: Location;
    param: string;
    value: string
    msg: string;
}

export interface OAuth2Token {
    /**
     * The access token string as issued by the authorization server.
     */
    access_token: string;
    /**
     * The type of token this is, typically just the string “bearer”.
     */
    token_type: 'Bearer';
    /**
     * If the access token expires, the server should reply with the duration of time the access token is granted for.
     *
     * This time is in seconds.
     */
    expires_in: number;
    /**
     * If the access token will expire, then it is useful to return a refresh
     * token which applications can use to obtain another access token.
     *
     * However, tokens issued with the implicit grant cannot be issued a refresh token.
     */
    refresh_token: string;
    /**
     * If the scope the user granted is identical to the scope the app requested, this parameter is optional.
     * If the granted scope is different from the requested scope, such as if the user modified the scope,
     * then this parameter is required.
     */
    scope?: '*' | string;
}

// region namespace: API
export declare namespace API {
    export type DateTime = string;
    export type Nullable<T> = T | null;
    export type HTML = string;

    /**
     * An object containing information about a field
     * that failed validation during an API request.
     *
     * @template Request the shape of the request that was called.
     */
    interface FieldValidationError<RequestParam extends PropertyKey = string> {
        param: RequestParam;
        message: string;
    }

    /**
     * An object containing information about a request made
     * to the api that failed validation in some manner.
     */
    interface ValidationErrorResponse<Request extends Record<string, unknown> = Record<string, unknown>> {
        errors: Array<FieldValidationError<keyof Request>>
    }

    // region namespace: Entities
    export namespace Entities {

        export interface Common {
            id: string;
            createdAt: DateTime;
            updatedAt: Nullable<DateTime>;
            deletedAt: Nullable<DateTime>;
        }

        interface CurrentUser extends User {

        }

        interface User extends Common {
            /**
             * The email of this `User`.
             */
            email: string;
            /**
             * The first name of this `User`.
             */
            firstName: string;
            /**
             * The last name of this `User`.
             */
            lastName: string;
            /**
             * Array of `id`s of the `Page`s
             * that belong to this `User`.
             */
            pages: Array<Page['id']>;
            /**
             * Array of `id`s of the `Notebook`s
             * this `User` has access to.
             */
            notebooks: Array<Notebook['id']>;
        }

        interface Notebook extends Common {
            /**
             * The label of this `Notebook`.
             */
            label: string;
            /**
             * The color of this `Notebook`.
             */
            color: string;
            /**
             * Collection of `tags` attached
             * to this `Notebook`, for sorting.
             */
            tags: Array<string>;
            /**
             * Array of `pageId`s of the `Page`s
             * that belong to this `Notebook`.
             */
            pages: Array<Page['id']>;
            /**
             * `id` of the `User` that owns this `Notebook`.
             *
             * A `Notebook` can only be deleted by it's `owner`.
             */
            owner: User['id'];
        }

        interface Page extends Common {
            /**
             * The title of this `Page`.
             */
            title: string;
            /**
             * The content of this `Page`.
             */
            content: string;
            /**
             * `id` of the `User` that owns this `Page`.
             *
             * A `Page` can only be deleted by it's `owner`.
             */
            owner: User['id'];
        }

        interface File {
            id: string;
            size: number;
            mimeType: string;
            url: string;
        }
    }
    // endregion

    // region namespace: <CollectionName>
    /*namespace CollectionName {
        // region <EndpointName>
        namespace EndpointName {
            type ENDPOINT = '/<route>';
            type METHOD = '<GET|POST|PUT|DELETE|ETC>';

            /!**
             * The parameters required for the endpoint.
             *!/
            interface Parameters {
                // endpoint route parameters go here, without the ':'
            }

            /!**
             * A request for <resource>.
             *!/
            interface Request {
                // request body properties go here
            }

            /!**
             * The response to a request for <resource>.
             *!/
            interface Response {
                // the structure of the endpoints typical response goes here
            }
        }
        // endregion
    }*/
    // endregion
    // region namespace: Authorization
    export namespace Authorization {
        // region ForgotPassword
        /**
         * Request an email to reset the password for an account if it has been forgotten.
         *
         * For security reasons this will always result in a `201 created` response no matter
         * if an account exists or not.
         */
        namespace ForgotPassword {
            type ENDPOINT = '/api/password/email';
            type METHOD = 'POST';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
            }

            /**
             * A request for the sending of an email for resetting the password of an account.
             */
            interface Request {
                /**
                 * The email address of the account whose password has been forgotten.
                 */
                email: string;
            }

            /**
             * A response to a request for the sending of an email for resetting the password of an account.
             */
            interface Response {
                data: {
                    email: string;
                    expiresIn: number;
                }
            }
        }
        // endregion
        // region ResetPassword
        /**
         * Use a temporary token received via email to change the password for an existing account.
         */
        namespace ResetPassword {
            type ENDPOINT = '/api/password/reset';
            type METHOD = 'POST';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
            }

            /**
             * Use a temporary token received via email to change the password for an existing account.
             */
            interface Request {
                /**
                 * The email address the password has been forgotten for.
                 */
                email: string;
                /**
                 * Token received via email to authenticate the reset request.
                 */
                token: string;
                /**
                 * A new password which suffices the requirements.
                 */
                password: string;
                /**
                 * The confirmation of the password.
                 */
                passwordConfirmation: string;
            }

            /**
             * For security reasons, this will always be a `201 Created`.
             */
            interface Response {
                // empty response
            }
        }
        // endregion
        // region RequestAccessToken
        export namespace RequestAccessToken {
            type ENDPOINT = '/oauth/token';
            type METHOD = 'POST';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            type GrantType =
                | 'refresh_token'
                | 'password'
                ;

            type GrantTypeRequestMap = {
                'refresh_token': RefreshTokenGrantRequest,
                'password': PasswordGrantRequest,
            }

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
            }

            /**
             * Request an access token with the specified grant_type.
             */
            type Request = RefreshTokenGrantRequest | PasswordGrantRequest;

            /**
             * Response for an access token of a specified grant_type.
             */
            interface Response extends OAuth2Token {
                /**
                 * @inheritDoc
                 */
                access_token: string;
                /**
                 * @inheritDoc
                 */
                token_type: 'Bearer';
                /**
                 * @inheritDoc
                 */
                expires_in: number;
                /**
                 * @inheritDoc
                 */
                refresh_token: string;
            }

            // region namespace: Common
            /**
             * Common properties in all `RequestAccessToken` requests & responses.
             */
            namespace Common {
                /**
                 * Request an access token with the specified grant_type.
                 */
                interface Request {
                    /**
                     * The OAuth grant type.
                     */
                    grant_type: GrantType;
                    /**
                     * The id of the client to use.
                     */
                    client_id: number;
                    /**
                     * Always request all scopes ('*')
                     */
                    scope: '*' | string;
                }
            }

            // endregion
            /**
             * The extend the time of the session, refresh the short-lived access token using the long-lived refresh-token.
             */
            interface RefreshTokenGrantRequest extends RequestAccessToken.Common.Request {
                /**
                 * @inheritDoc
                 */
                grant_type: 'refresh_token';
                /**
                 * @inheritDoc
                 *
                 * Will always be '1' for `refresh_token` grants.
                 */
                client_id: 1;
                /**
                 * The refresh token from the original auth request.
                 */
                refresh_token: string;
            }

            /**
             * Request an access token with user name and password. Access tokens are shortlived (tbd).
             */
            interface PasswordGrantRequest extends RequestAccessToken.Common.Request {
                /**
                 * @inheritDoc
                 */
                grant_type: 'password';
                /**
                 * @inheritDoc
                 *
                 * Will always be '2' for password grants.
                 */
                client_id: 1;
                /**
                 * @inheritDoc
                 */
                scope: '*' | string;
                /**
                 * Email address of the user.
                 */
                username: string;
                /**
                 * Password of the user.
                 */
                password: string;
            }
        }
        // endregion
    }
    // endregion
    // region namespace: Users
    /**
     *
     */
    namespace Users {
        // region GetCurrentUser
        /**
         * Shows the currently authenticated `User`.
         */
        namespace GetCurrentUser {
            type ENDPOINT = '/api/user';
            type METHOD = 'GET';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
            }

            /**
             * A request to show the currently authenticated `User`.
             */
            interface Request {
            }

            /**
             * A response to a request to show the currently authenticated `User`.
             */
            interface Response {
                data: Entities.CurrentUser;
            }
        }
        // endregion
        // region ShowSingleUser
        /**
         * Retrieve information of a single `User`.
         */
        namespace ShowSingleUser {
            type ENDPOINT = '/api/users/:userId';
            type METHOD = 'GET';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
                /**
                 * The internal id/slug of the `User`.
                 */
                userId: string;
            }

            /**
             * A request for the information of a single `User`.
             */
            interface Request {
            }

            /**
             * A response to a request for the information of a single `User`.
             */
            interface Response {
                data: Entities.User;
            }
        }
        // endregion
        // region CreateUser
        /**
         * Signs a new `User` up.
         */
        namespace CreateUser {
            type ENDPOINT = '/api/users';
            type METHOD = 'POST';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
            }

            /**
             * A request to sign a new `User` up.
             */
            interface Request {
                /**
                 * The email address of the `User`.
                 */
                email: string;
                /**
                 * The first name of the `User`.
                 */
                firstName: string;
                /**
                 * The last name of the `User`.
                 */
                lastName: string;
                /**
                 * The password of the `User`.
                 */
                password: string;
                /**
                 * The confirmation text given by the `User` to confirm
                 * that they entered their password correctly.
                 */
                passwordConfirmation: string;
                /**
                 * Represents if the `User` has accepted the terms & conditions.
                 *
                 * The server will reject this request if this isn't `true`.
                 */
                terms: boolean;
            }

            /**
             * A response to a request to sign a new `User` up.
             */
            interface Response {
                data: Entities.User;
            }
        }
        // endregion
        // region UpdateUser
        /**
         * Update the details of a `User`.
         *
         * This endpoint updates single fields of the `User`,
         * and any missing fields in the request will be ignored.
         * To remove a field from the `User` profile, explicitly set it to null.
         *
         * When updating the `User`'s password,
         * the current password is required to approve the request.
         */
        namespace UpdateUser {
            type ENDPOINT = '/api/users/:userId';
            type METHOD = 'PATCH';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
                /**
                 * The internal id/slug of the `User`.
                 */
                userId: string;
            }

            /**
             * A request to update a `User`.
             */
            interface Request {
                /**
                 * The email address of the `User`.
                 */
                email?: string;
                /**
                 * The first name of the `User`.
                 */
                firstName?: string;
                /**
                 * The last name of the `User`.
                 */
                lastName?: string;
                /**
                 * The password of the `User`.
                 */
                password?: string;
                /**
                 * The confirmation text given by the `User` to confirm
                 * that they entered their password correctly.
                 */
                passwordConfirmation?: string;
                /**
                 * Required when the `password` is changed.
                 */
                passwordCurrent?: string;
            }

            /**
             * A response to a request to update a `User`.
             */
            interface Response {
                data: Entities.User;
            }
        }
        // endregion
        // region RemoveUser
        /**
         * Remove a `User`.
         */
        namespace RemoveUser {
            type ENDPOINT = '/api/users/:userId';
            type METHOD = 'DELETE';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
                /**
                 * The id of the `User` to delete.
                 */
                userId: string;
            }

            /**
             * A request to delete a `User`.
             */
            interface Request {
            }

            /**
             * A response to a request to delete a `User`.
             */
            interface Response {
            }
        }
        // endregion
    }
    // endregion
    // region namespace: Files
    namespace Files {
        // region UploadFile
        /**
         * Uploads a `File`.
         */
        namespace UploadFile {
            type ENDPOINT = '/api/files';
            type METHOD = 'POST';

            type KeyOfParameters = keyof Parameters;
            type KeyOfRequest = keyof Request;
            type KeyOfResponse = keyof Response;

            /**
             * The parameters required to make a request to this endpoint.
             */
            interface Parameters {
            }

            /**
             * A request to upload a `File`.
             */
            interface Request {
                /**
                 * The `File` to upload.
                 */
                file: unknown;
            }

            /**
             * A response to a request to upload a `File`.
             */
            interface Response {
                data: Entities.File;
            }
        }
        // endregion
    }
    // endregion
}
// endregion
