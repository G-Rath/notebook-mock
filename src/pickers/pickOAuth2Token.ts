import { OAuth2Token } from 'src/definitions';
import pickFrom from 'src/utilities/pickFrom';
import { keys } from 'ts-transformer-keys/index';

/**
 * Picks out the `OAuth2Token` object from the given `object`,
 * ensuring that the `OAuth2Token` object contains no additional properties.
 *
 * @param {OAuth2Token} object
 *
 * @return {OAuth2Token}
 */
export default (object: OAuth2Token): OAuth2Token => pickFrom(object, keys<OAuth2Token>());
