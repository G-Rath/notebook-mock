import { API } from 'src/definitions';
import pickFrom from 'src/utilities/pickFrom';
import { keys } from 'ts-transformer-keys/index';

/**
 * Picks out the `CurrentUser` entity object from the given `object`,
 * ensuring that the `CurrentUser` object contains no additional properties.
 *
 * @param {API.Entities.CurrentUser} object
 *
 * @return {API.Entities.CurrentUser}
 */
export default (object: API.Entities.CurrentUser): API.Entities.CurrentUser => pickFrom(object, keys<API.Entities.CurrentUser>());
