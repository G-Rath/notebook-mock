import { API } from 'src/definitions';
import pickFrom from 'src/utilities/pickFrom';
import { keys } from 'ts-transformer-keys/index';

/**
 * Picks out the `User` entity object from the given `user` object,
 * ensuring that the `User` object contains no additional properties.
 *
 * @param {API.Entities.User} object
 *
 * @return {API.Entities.User}
 */
export default (object: API.Entities.User): API.Entities.User => pickFrom(object, keys<API.Entities.User>());
