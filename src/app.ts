import * as http from 'http';
import * as HttpStatus from 'http-status-codes';
import Db from 'src/database';
import routes from 'src/routes';
import express = require('express');
import createError = require('http-errors');
import logger = require('morgan');
import delay = require('express-delay');
import bearerToken = require('express-bearer-token');

process.on('unhandledRejection', r => console.error(r)); // gives us the line number on unhandled promise rejections

// @ts-ignore
process._debugProcess(process.pid);

const app = express();

app.set('dbLoadedPromise', Db.reconfigureDb());

app.use(bearerToken());

const allowCrossDomain = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Clacks-Overhead');

    // intercept OPTIONS method
    if (req.method.toUpperCase() === 'OPTIONS') {
        res.status(HttpStatus.OK).json();

        return;
    }

    next();
};
app.use(allowCrossDomain);

app.use(delay(
    Number(process.env.DELAY_MIN) || 100,
    Number(process.env.DELAY_MAX) || 1000
));

app.use(logger('dev', {
    skip: (req: http.IncomingMessage) => req['noLog'] || req.url === '/live' // if noLog is set, then don't log this request
}));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/live', (req: express.Request, res: express.Response) => res.status(HttpStatus.OK).json());
app.use('/', routes);

app.use(async (req, res, next) => {
    console.log('db saved');
    await Db.save();
    next();
});

// catch 404 and forward to error handler
app.use((req, res, next) => next(createError(HttpStatus.NOT_FOUND)));

// error handler
app.use((err: any, req: express.Request, res: express.Response) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    if (req.app.get('env') === 'development' && err.status !== HttpStatus.NOT_FOUND) {
        console.log(err);
    }

    // render the error page
    res.status(err.status || HttpStatus.INTERNAL_SERVER_ERROR);
    res.json();
});

export = app;
