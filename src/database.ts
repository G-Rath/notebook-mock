import LokiJS from 'lokijs';
import { OAuth2Token } from 'src/definitions';

/**
 * Gets the path of the db file.
 *
 * Defined as an internal `const` to allow usage
 * before definition in `Db` class.
 *
 * @return {string}
 * @private
 */
const _getDbFilePath = () => process.env.DB_FiLE_PATH || 'db.json';

class Db {
    private static _db = new LokiJS(_getDbFilePath());

    public static reconfigureDb(options: Partial<Loki['options']> = {}, filePath: string = this.dbFilePath): Promise<void> {
        this._db = new LokiJS(filePath, options);

        return this.load();
    }

    public static load(): Promise<void> {
        return new Promise((resolve, reject) => {
            this._db.loadDatabase({}, err => {
                if (err) {
                    console.error(`failed to load database from ${this._db.filename}`);

                    return reject(err);
                }

                return resolve();
            });
        });
    }

    /**
     * Promises that the database has been fully saved to file.
     *
     * @return {Promise<void>}
     * @public
     * @static
     */
    public static save(): Promise<void> {
        return new Promise((resolve, reject) => {
            this._db.saveDatabase(err => {
                if (err) {
                    console.error(`failed saving database to ${this._db.filename}`);

                    return reject(err);
                }

                // console.log(`successfully saved database to ${this._db.filename}`);
                return resolve();
            });
        });
    }

    public static get dbFilePath(): string {
        return _getDbFilePath();
    }

    public static get tokens(): Collection<Db.AccessToken> {
        return this._db.addCollection('tokens', {
            disableMeta: true,
            indices: ['userId', 'access_token', 'refresh_token']
        });
    }

    public static get users(): Collection<Db.User> {
        return this._db.addCollection('users', {
            disableMeta: true,
            indices: ['id', 'email']
        });
    }

    public static get notebooks(): Collection<Db.Notebook> {
        return this._db.addCollection('notebooks', {
            disableMeta: true,
            indices: ['id']
        });
    }

    public static get pages(): Collection<Db.Page> {
        return this._db.addCollection('pages', {
            disableMeta: true,
            indices: ['id']
        });
    }
}

namespace Db {
    type DateTime = string;
    type Nullable<T> = T | null;

    export interface Common {
        id: string;
        createdAt: DateTime;
        updatedAt: Nullable<DateTime>;
        deletedAt: Nullable<DateTime>;
    }

    export interface AccessToken extends OAuth2Token, Common {
        userId: User['id'];
    }

    export interface User extends Common {
        /**
         * The email of this `User`.
         */
        email: string;
        /**
         * The first name of this `User`.
         */
        firstName: string;
        /**
         * The last name of this `User`.
         */
        lastName: string;
        /**
         * The password of this `User`.
         */
        password: string;
        /**
         * A 32-bit string used to hash the `password` of a `User`.
         */
        salt: string;
        /**
         * Array of `id`s of the `Page`s
         * that belong to this `User`.
         */
        pages: Array<Page['id']>;
        /**
         * Array of `id`s of the `Notebook`s
         * this `User` has access to.
         */
        notebooks: Array<Notebook['id']>;
    }

    export interface Notebook extends Common {
        /**
         * The label of this `Notebook`.
         */
        label: string;
        /**
         * The color of this `Notebook`.
         */
        color: string;
        /**
         * Collection of `tags` attached
         * to this `Notebook`, for sorting.
         */
        tags: Array<string>;
        /**
         * Array of `pageId`s of the `Page`s
         * that belong to this `Notebook`.
         */
        pages: Array<Page['id']>;
        /**
         * `id` of the `User` that owns this `Notebook`.
         *
         * A `Notebook` can only be deleted by it's `owner`.
         */
        owner: User['id'];
    }

    export interface Page extends Common {
        /**
         * The title of this `Page`.
         */
        title: string;
        /**
         * The content of this `Page`.
         */
        content: string;
        /**
         * `id` of the `User` that owns this `Page`.
         *
         * A `Page` can only be deleted by it's `owner`.
         */
        owner: User['id'];
    }
}

export default Db;

//
// export const seedDb = () => {
//     const seller: DB.Entities.Seller = {
//         id: '0',
//         email: 'Bi1boR0ckz28@theshire.nz',
//         emailConfirmed: true,
//         hasBeenToured: true,
//         firstName: 'Bilbo',
//         lastName: 'Baggins',
//         team: 'whale-hunters',
//         company: 'the-fellowship',
//         password: 'not-safe',
//         createdAt: '2018-06-29T01:26:33+00:00',
//         updatedAt: '2018-06-29T01:34:17+00:00',
//         deletedAt: null
//     };
//
//     const styles: DB.Entities.Seller.Styles = {
//         sellerId: seller.id,
//         hostingUrl: null,
//         textBodyColor: null,
//         textBodyFont: null,
//         textHeadingColor: null,
//         textHeadingFont: null,
//         textQuoteColor: null,
//         textQuoteFont: null,
//         textSmallColor: null,
//         textSmallFont: null,
//         pageBackgroundColor: null,
//         pagePrimaryColor: null,
//         pageSecondaryColor: null,
//         pageTertiaryColor: null,
//         logoUrl: null
//     };
//
//     const persona: DB.Entities.Persona = {
//         id: 'ring-makers',
//         name: 'Rings for Sale',
//         sellerId: seller.id,
//         createdAt: '2018-06-29T01:26:57+00:00',
//         updatedAt: '2018-06-29T01:26:57+00:00',
//         deletedAt: null
//     };
//
//     const persona2: DB.Entities.Persona = {
//         id: 'weapon-dealings',
//         name: 'Weapons! Weapons! Weapons!',
//         sellerId: seller.id,
//         createdAt: '2018-06-29T01:26:57+00:00',
//         updatedAt: '2018-06-29T01:26:57+00:00',
//         deletedAt: null
//     };
//
//     const steps: Array<DB.Entities.Step> = [
//         {
//             personaId: persona.id,
//             id: 'loosen-status-quo',
//             name: 'Loosen status quo',
//             position: 1
//         },
//         {
//             personaId: persona.id,
//             id: 'commit-to-change',
//             name: 'Commit to change',
//             position: 2
//         },
//         {
//             personaId: persona.id,
//             id: 'explore-solutions',
//             name: 'Explore solutions',
//             position: 3
//         }
//     ];
//
//     DbM.sellersCollection.insert(seller);
//     DbM.stylesCollection.insert(styles);
//     DbM.personasCollection.insert(persona);
//     DbM.personasCollection.insert(persona2);
//     DbM.stepsCollection.insert(steps);
// };
/*
"{"id":"7","imageUrl":null,"videoUrl":"","title":"my story","summary":"","body":"<h2>“Less desugaring” == more sweets?</h2>\n<p>Perhaps surprisingly, we had to spend quite a bit of effort on getting seemingly simple unary operations, like <code>-x</code>, to work. So far, <code>-x</code> did exactly the same as <code>x * (-1)</code>, so to simplify things, V8 applied precisely this replacement as early as possible when processing JavaScript, namely in the parser. This approach is called “desugaring”, because it treats an expression like <code>-x</code> as “syntactic sugar” for <code>x * (-1)</code>. Other components (the interpreter, the compiler, the entire runtime system) didn’t even need to know what a unary operation is, because they only ever saw the multiplication, which of course they must support anyway.</p>\n<p><br></p>\n<p>With BigInts, however, this implementation suddenly becomes invalid, because multiplying a BigInt with a Number (like <code>-1</code>) must throw a <code>TypeError</code>4. The parser would have to desugar <code>-x</code> to <code>x * (-1n)</code> if <code>x</code> is a BigInt — but the parser has no way of knowing what <code>x</code> will evaluate to. So we had to stop relying on this early desugaring, and instead add proper support for unary operations on both Numbers and BigInts everywhere.</p>\n<p>4 Mixing <code>BigInt</code> and <code>Number</code> operand types is generally not allowed. That’s somewhat unusual for JavaScript, but there is <a href=\"https://developers.google.com/web/updates/2018/05/bigint#operators\">an explanation</a> for this decision.</p>\n<figure><img src=\"https://s3-ap-southeast-2.amazonaws.com/auto-storage/mock-images/cd58b2ff-2eab-4499-bddb-a317ce03c621.jpg\"/></figure>\n<h2>A bit of fun with bitwise ops</h2>\n<p>Most computer systems in use today store signed integers using a neat trick called “two’s complement”, which has the nice properties that the first bit indicates the sign, and adding 1 to the bit pattern always increments the number by 1, taking care of the sign bit automatically. For example, for 8-bit integers:</p>\n<ul>\n  <li><code>10000000</code> is -128, the lowest representable number,</li>\n  <li><code>10000001</code> is -127,</li>\n  <li><code>11111111</code> is -1,</li>\n  <li><code>00000000</code> is 0,</li>\n  <li><code>00000001</code> is 1,</li>\n  <li><code>01111111</code> is 127, the highest representable number.</li>\n</ul>\n<p>This encoding is so common that many programmers expect it and rely on it, and the BigInt specification reflects this fact by prescribing that BigInts must act as if they used two’s complement representation. As described above, V8’s BigInts don’t!</p>\n<p><br></p>\n<p>To perform bitwise operations according to spec, our BigInts therefore must pretend to be using two’s complement under the hood. For positive values, it doesn’t make a difference, but negative numbers must do extra work to accomplish this. That has the somewhat surprising effect that <code>a &amp; b</code>, if <code>a</code> and <code>b</code> are both negative BigInts, actually performs <em>four</em> steps (as opposed to just one if they were both positive): both inputs are converted to fake-two’s-complement format, then the actual operation is done, then the result is converted back to our real representation. Why the back-and-forth, you might ask? Because all the non-bitwise operations are much easier that way.</p>\n<p><br></p>","stepId":"0","createdAt":"2018-11-19T23:38:03.611Z","updatedAt":"2018-11-19T23:38:09.505Z","deletedAt":null,"personaId":"ring-makers","consumerId":"2","position":1,"$loki":8}"
 */
