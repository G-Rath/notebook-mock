import express = require('express');
import oauth from 'src/routes/oauth';
import user from 'src/routes/user';

/** @type {IRouter} */
const router = express.Router();

router.use('/oauth', oauth);
router.use('/api/user', user);

export = router;
