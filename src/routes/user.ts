import express = require('express');
import { Request, Response } from 'express-serve-static-core';
import * as HttpStatus from 'http-status-codes';
import { API } from 'src/definitions';
import { pickApiCurrentUser } from 'src/pickers';
import authRequestHandler from 'src/routes/request-handlers/authRequestHandler';

/** @type {IRouter} */
const user = express.Router({ mergeParams: true });

// region GetCurrentUser
/**
 * Shows the currently authenticated `User`.
 */
user.get('/', authRequestHandler, (req: Request, res: Response) => {
    const body: API.Users.GetCurrentUser.Response = {
        data: pickApiCurrentUser(req.currentDbUser)
    };

    return res.status(HttpStatus.OK).json(body);
});
// endregion

export default user;
