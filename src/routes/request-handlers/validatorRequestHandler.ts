import { RequestHandler } from 'express-serve-static-core';
import { validationResult } from 'express-validator/check';
import * as HttpStatus from 'http-status-codes';
import { API } from 'src/definitions';
import errorFormatter from 'src/utilities/errorFormatter';

/**
 * `RequestHandler` for handling validator errors provided by `express-validator`.
 */
const validatorRequestHandler: RequestHandler = (req, res, next): any => {
    const errors = validationResult<API.FieldValidationError>(req);

    if (!errors.isEmpty()) {
        const body: API.ValidationErrorResponse = {
            errors: errors.formatWith(errorFormatter).array()
        };

        return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json(body);
    }

    next();
};

export default validatorRequestHandler;
