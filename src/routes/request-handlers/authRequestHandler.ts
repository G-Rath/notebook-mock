import { RequestHandler } from 'express-serve-static-core';
import * as HttpStatus from 'http-status-codes';
import Db from 'src/database';
import { hasAccessTokenExpired } from 'src/utilities';

declare global {
    namespace Express {
        export interface Request {
            token?: string;
            currentDbUser: Db.User;
        }
    }
}
/**
 * `RequestHandler` for handling authentication of routes.
 */
const authRequestHandler: RequestHandler = async (req, res, next): Promise<any> => {
    const accessToken = await Db.tokens.findOne({ access_token: req.token });

    if (accessToken === null || accessToken.deletedAt || hasAccessTokenExpired(accessToken)) {
        return res.status(HttpStatus.UNAUTHORIZED).json();
    } // if the accessToken doesn't exist, is deleted, or expired, it's invalid

    const currentDbUser = await Db.users.findOne({ id: accessToken.userId });

    if (currentDbUser === null) {
        throw new Error(`no user exists with the id "${accessToken.id}"`);
    } // accessTokens shouldn't point to users that don't exist

    req.currentDbUser = currentDbUser;

    next();
};

export default authRequestHandler;
