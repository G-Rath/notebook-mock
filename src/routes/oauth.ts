import express = require('express');
import * as HttpStatus from 'http-status-codes';
import Db from 'src/database';
import { API } from 'src/definitions';
import { pickOAuth2Token } from 'src/pickers';
import { generateStringToken, hashPassword } from 'src/utilities';

/** @type {IRouter} */
const oauth = express.Router({ mergeParams: true });

/**
 * Gets the time in seconds to expire access tokens after their creation date.
 *
 * @return {number}
 */
const getTokenExpiresAfterValue = () => parseFloat(process.env.EXPIRE_TOKEN_AFTER || '') || 800;

/*
    accessToken rules:

    -> `password` grant request results in a new access/refresh token pair.
    -> use of a `refresh_token` invalidates it's paired `access_token`,
        -> other access/refresh token pairs are untouched, regardless of user.

    -> when an access_token expires, it's no longer usable, but it's refresh_token is.
 */
// region RequestAccessToken
oauth.post('/token', (req, res) => {
    // todo: try and implement validation using express-validator
    const {} = req.params as API.Authorization.RequestAccessToken.Parameters;

    const request = req.body as API.Authorization.RequestAccessToken.Request;

    let finalAccessToken: API.Authorization.RequestAccessToken.Response | null = null;

    switch (request.grant_type) {
        case 'password': {
            const user = Db.users.findOne({ email: request.username });

            if (
                user === null || user.deletedAt
                || user.password !== hashPassword(request.password, user.salt)
            ) {
                return res
                    .status(HttpStatus.BAD_REQUEST)
                    .json({
                        error: 'invalid_grant',
                        error_description: 'invalid password'
                    }); // if the given password doesn't match the users password...
            }  // ... then it's an invalid password, so return an error.

            const accessToken = Db.tokens.insertOne({
                id: `${Db.tokens.maxId}`,
                userId: user.id,
                scope: '*',
                token_type: 'Bearer',
                refresh_token: generateStringToken(),
                access_token: generateStringToken(),
                expires_in: getTokenExpiresAfterValue(),
                createdAt: (new Date()).toISOString(),
                updatedAt: null,
                deletedAt: null
            }); // generate a new `Bearer` access token

            if (accessToken === undefined) {
                throw new Error('a database error occurred while trying to insert accessToken');
            } // shouldn't ever happen..? but test just to be safe

            finalAccessToken = { ...accessToken };
            break;
        }
        case 'refresh_token': {
            const accessToken = Db.tokens.findOne({ refresh_token: request.refresh_token });

            if (accessToken === null) {
                return res
                    .status(HttpStatus.BAD_REQUEST)
                    .json({
                        error: 'invalid_grant',
                        error_description: 'invalid refresh_token'
                    }); // if no access token exists with the given refresh_token...
            } // ... then it's an invalid refresh_token, so return an error.

            const updatedAccessToken = Db.tokens.update({
                ...accessToken,
                access_token: generateStringToken(),
                refresh_token: generateStringToken(),
                updatedAt: (new Date()).toISOString()
            }); // invalid the old refresh_token too

            finalAccessToken = { ...updatedAccessToken };
            break;
        }
        default:
            return res
                .status(HttpStatus.BAD_REQUEST)
                .json({
                    error: 'unsupported_grant_type',
                    error_description: `"${(request as API.Authorization.RequestAccessToken.Request).grant_type}" is not a supported grant_type`
                });
    }

    const body: API.Authorization.RequestAccessToken.Response = pickOAuth2Token(finalAccessToken);

    return res.status(HttpStatus.OK).json(body);
});
// endregion

export default oauth;
