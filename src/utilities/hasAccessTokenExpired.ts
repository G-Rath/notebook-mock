import Db from 'src/database';

/**
 * Checks if the given `AccessToken` has expired or not.
 *
 * @param {number} expires_in
 * @param {Db.Nullable<Db.DateTime>} updatedAt
 * @param {Db.DateTime} createdAt
 *
 * @return {boolean}
 */
export default ({ expires_in, updatedAt, createdAt }: Db.AccessToken): boolean => (expires_in * 1000) + Date.parse(updatedAt || createdAt) <= Date.now()
