/**
 * Returns `value` unless `undefined`, in which case returns `unless`.
 *
 * @param {T?} value
 * @param {T} unless
 *
 * @template T
 */
export default <T>(value: T | undefined, unless: T): T => {
    if (value !== undefined) {
        return value;
    }

    return unless;
};
