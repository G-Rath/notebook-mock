/**
 * Returns an object containing all the properties in the given `object`,
 * except for the ones whose names are not included in the `keys` array.
 *
 * @param {object|T} object
 * @param {Array<string|K>} keys
 *
 * @return {object|{[k in K]: T[k]}}
 *
 * @template T
 * @template K
 */
export default <T, K extends keyof T = keyof T>(object: T, keys: Array<K & keyof T>): Pick<T, K> => {
    const obj = {} as Pick<T, K>;

    keys.forEach(key => obj[key] = object[key]);

    return obj;
};
