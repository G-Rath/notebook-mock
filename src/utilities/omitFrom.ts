import { Omit } from 'src/definitions';

/**
 * Returns an object containing all the properties in the given `object`,
 * except for the ones whose names are included in the `keys` array.
 *
 * @param {object|T} object
 * @param {Array<string|K>} keys
 *
 * @return {object|{[k in K]: T[k]}}
 *
 * @template T
 * @template K
 */

export default <T, K extends keyof T>(object: T, keys: Array<K>): Omit<T, K> => {
    const obj = { ...object };

    keys.forEach(key => delete obj[key]);

    return obj;
};
