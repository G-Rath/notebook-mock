import { ErrorFormatter } from 'express-validator/check';
import { API, UnformattedError } from 'src/definitions';

/**
 * Error formatter for formatting errors.
 *
 * @param {UnformattedError} error
 *
 * @return {API.FieldValidationError}
 */
const errorFormatter: ErrorFormatter<API.FieldValidationError> = (error: UnformattedError): API.FieldValidationError => ({
    param: error.param,
    message: error.msg
});

export default errorFormatter;
