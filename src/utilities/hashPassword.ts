import crypto from 'crypto';

/**
 * Hashes the given `password` against the given `salt`.
 *
 * @param {string} password
 * @param {string} salt
 *
 * @return {string}
 */
export default (password: string, salt: string): string => crypto.pbkdf2Sync(password, salt, 10_000, 32, 'sha512').toString('base64');
