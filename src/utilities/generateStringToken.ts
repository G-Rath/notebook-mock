import crypto from 'crypto';

/**
 * Generates a string usable as a `token` or `salt`.
 *
 * @return {string}
 */
export default (): string => crypto.randomBytes(32).toString('base64');
